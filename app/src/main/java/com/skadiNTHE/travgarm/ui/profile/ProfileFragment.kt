package com.skadiNTHE.travgarm.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.skadiNTHE.travgarm.R
import com.skadiNTHE.travgarm.databinding.FragmentProfileBinding
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation


public class ProfileFragment : Fragment() {
    private var _binding: FragmentProfileBinding? = null
    private lateinit var auth: FirebaseAuth

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        auth = Firebase.auth
        _binding = FragmentProfileBinding.inflate(inflater, container, false)

        binding.btnLogout.setOnClickListener { _ ->
            run {
                Firebase.auth.signOut()

                // TODO: pindah ke logout activity
                activity?.finish()
            }
        }

        return binding.root
    }

    override fun onStart() {
        super.onStart()

        val currentUser = auth.currentUser
        if (currentUser != null) {
            binding.emailName.text = currentUser.displayName
            Picasso.get()
                .load(currentUser.photoUrl)
                .transform(CropCircleTransformation())
                .placeholder(R.mipmap.ic_profile_placeholder_round)
                .into(binding.profileImage);
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
